module.exports = {
  theme: {
    extend: {},
    colors: {
      "grey-900": "#212121",
      "grey-800": "#424242",
      "grey-700": "#616161",
      "grey-600": "#757575",
      "grey-500": "#9E9E9E",
      "grey-400": "#BDBDBD",
      "grey-300": "#E0E0E0",
      "grey-200": "#EEEEEE",
      "grey-100": "#F5F5F5",

      "red-700": "#d32f2f",

      black: "#22292f",
      white: "#ffffff"
    },
    fontFamily: {
      sans: [
        "-apple-system",
        "BlinkMacSystemFont",
        '"Segoe UI"',
        "Roboto",
        '"Helvetica Neue"',
        "Arial",
        '"Noto Sans"',
        "sans-serif",
        '"Apple Color Emoji"',
        '"Segoe UI Emoji"',
        '"Segoe UI Symbol"',
        '"Noto Color Emoji"'
      ],
      serif: [
        "roboto_slabregular",
        "Georgia",
        "Cambria",
        '"Times New Roman"',
        "Times",
        "serif"
      ],
      mono: [
        "Menlo",
        "Monaco",
        "Consolas",
        '"Liberation Mono"',
        '"Courier New"',
        "monospace"
      ]
    },
    screens: {
      lg: "1024px"
    }
  },
  variants: {},
  plugins: []
}
