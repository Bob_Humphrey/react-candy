import React, { Component } from "react"
import Data from "./components/Data"
import Sort from "./components/Sort"
import Filter from "./components/Filter"
import "./css/tailwind.css"

const dogImg = require("./dog.jpg")
const logo = require("./bh-logo.gif")
const candies = require("./candy.json")

class App extends Component {
  constructor(props) {
    super(props)

    this.state = {
      sortField: "competitorname",
      sortOrder: "asc",
      filter: "none"
    }
  }

  handleSortChange = changeEvent => {
    let sortOrder
    switch (changeEvent.target.value) {
      case "competitorname":
        sortOrder = "asc"
        break
      case "winpercent":
        sortOrder = "desc"
        break
      case "pricepercent":
        sortOrder = "desc"
        break
      case "sugarpercent":
        sortOrder = "asc"
        break
      default:
        sortOrder = "asc"
        break
    }

    this.setState({
      sortField: changeEvent.target.value,
      sortOrder: sortOrder
    })
  }

  handleFilterChange = changeEvent => {
    this.setState({
      filter: changeEvent.target.value
    })
  }

  render() {
    return (
      <div className="mt-4">
        <main className="lg:flex justify-center w-full mt-4">
          <div className="w-full lg:w-5/6 border-t-8 border-l-8 border-r-8 lg:border-b-8 border-grey-400">
            <div className="lg:flex">
              <div className="lg:flex justify-center lg:w-2/5 bg-grey-200">
                <div className="w-5/6 mt-10">
                  <img
                    className="hidden lg:block"
                    src={dogImg}
                    alt="Dog Smile Factory"
                  />
                  <div className="text-center lg:-m-12">
                    <h1 className="text-3xl font-bold text-grey-800 lg:text-white pt-4 lg:pt-0">
                      Halloween Candy
                    </h1>
                  </div>
                  <div className="lg:mt-12 p-4 text-left font-serif">
                    Analyze 83 different kinds of Halloween Candy in terms of
                    popularity, price, and sugar content. Based on &nbsp;
                    <a
                      className="text-red-700"
                      href="https://fivethirtyeight.com/features/the-ultimate-halloween-candy-power-ranking/"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      The Ultimate Halloween Candy Power Ranking
                    </a>
                    , using research and data provided by &nbsp;
                    <a
                      className="text-red-700"
                      href="https://fivethirtyeight.com/"
                      target="_blank"
                      rel="noopener noreferrer"
                    >
                      fivethirtyeight.com
                    </a>
                    .
                  </div>
                </div>
              </div>

              <div className="w-full lg:w-1/5 bg-grey-200 border-l-8 border-r-8 border-grey-400">
                <Sort
                  sortField={this.state.sortField}
                  sortOrder={this.state.sortOrder}
                  onChange={this.handleSortChange}
                />
                <Filter
                  filter={this.state.filter}
                  onChange={this.handleFilterChange}
                />
              </div>

              <Data
                candies={candies}
                sortField={this.state.sortField}
                sortOrder={this.state.sortOrder}
                filter={this.state.filter}
              />
            </div>
          </div>
        </main>

        <footer>
          <div className="w-full bg-grey-200 lg:hidden block border-b-8 border-l-8 border-r-8 lg:border-b-8 border-grey-400">
            <img className="" src={dogImg} alt="Dog Smile Factory" />
          </div>
          <div class="flex justify-center w-full bg-grey-400 mt-4 py-12">
            <figure class="w-16">
              <a href="https://bob-humphrey.com/">
                <img src={logo} alt="Bob Humphrey Web Development Logo" />
              </a>
            </figure>
          </div>
        </footer>
      </div>
    )
  }
}

export default App
