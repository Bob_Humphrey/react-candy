import React, { Component } from "react"

const _ = require("lodash")

class Data extends Component {
  render() {
    let filter = this.props.filter

    // Filter by any attribute.

    let filtered
    if (filter !== "none") {
      filtered = _.filter(this.props.candies, function(candy) {
        return candy[filter] === "1"
      })
    } else {
      filtered = _.cloneDeep(this.props.candies)
    }

    // Sort.

    const sorted = _.orderBy(
      filtered,
      this.props.sortField,
      this.props.sortOrder
    )

    // Add a rank property to each object.

    const ranked = sorted.map(function(candy, i) {
      candy.rank = i + 1
      return candy
    })

    // Chunk into an array of 2 for 2 columns.

    let chunks = []
    if (ranked.length <= 10) {
      chunks[0] = ranked
      chunks[1] = []
    } else {
      chunks = _.chunk(ranked, Math.ceil(ranked.length / 2))
    }

    return (
      <div className="w-full lg:w-2/5 lg:flex bg-grey-200">
        <div className="w-full lg:w-1/2 lg:p-4 px-4 pt-4">
          <ul className="list-reset">
            {chunks[0].map(candy => (
              <li key={candy.rank.toString()} className="mb-1 font-serif">
                {candy.rank + ". " + candy.competitorname}
              </li>
            ))}
          </ul>
        </div>
        <div className="w-full lg:w-1/2 lg:p-4 px-4 pb-4">
          <ul className="list-reset">
            {chunks[1].map(candy => (
              <li key={candy.rank.toString()} className="mb-1 font-serif">
                {candy.rank + ". " + candy.competitorname}
              </li>
            ))}
          </ul>
        </div>
      </div>
    )
  }
}

export default Data
