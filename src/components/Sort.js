import React, { Component } from "react"

class Sort extends Component {
  render() {
    return (
      <div className="bg-grey-200 p-4">
        <h3 className="h3-style">Sort</h3>
        <div className="mb-1 font-serif">
          <label>
            <input
              type="radio"
              name="sort-order"
              value="competitorname"
              checked={this.props.sortField === "competitorname"}
              onChange={this.props.onChange}
              className="mr-1"
            />
            Name
          </label>
        </div>

        <div className="mb-1 font-serif">
          <label>
            <input
              type="radio"
              name="sort-order"
              value="winpercent"
              checked={this.props.sortField === "winpercent"}
              onChange={this.props.onChange}
              className="mr-1"
            />
            Most Popular
          </label>
        </div>

        <div className="mb-1 font-serif">
          <label>
            <input
              type="radio"
              name="sort-order"
              value="pricepercent"
              checked={this.props.sortField === "pricepercent"}
              onChange={this.props.onChange}
              className="mr-1"
            />
            Most Expensive
          </label>
        </div>

        <div className="mb-1 font-serif">
          <label>
            <input
              type="radio"
              name="sort-order"
              value="sugarpercent"
              checked={this.props.sortField === "sugarpercent"}
              onChange={this.props.onChange}
              className="mr-1"
            />
            Least Sugar
          </label>
        </div>
      </div>
    )
  }
}

export default Sort
