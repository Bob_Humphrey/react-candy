import React, { Component } from "react"

class Filter extends Component {
  render() {
    return (
      <div className="p-4">
        <h3 className="h3-style">Filter</h3>

        <div className="mb-1 font-serif">
          <label>
            <input
              type="radio"
              name="filter"
              value="none"
              checked={this.props.filter === "none"}
              onChange={this.props.onChange}
              className="mr-1"
            />
            None
          </label>
        </div>

        <div className="mb-1 font-serif">
          <label>
            <input
              type="radio"
              name="filter"
              value="chocolate"
              checked={this.props.filter === "chocolate"}
              onChange={this.props.onChange}
              className="mr-1"
            />
            Chocolate
          </label>
        </div>

        <div className="mb-1 font-serif">
          <label>
            <input
              type="radio"
              name="filter"
              value="fruity"
              checked={this.props.filter === "fruity"}
              onChange={this.props.onChange}
              className="mr-1"
            />
            Fruity
          </label>
        </div>

        <div className="mb-1 font-serif">
          <label>
            <input
              type="radio"
              name="filter"
              value="caramel"
              checked={this.props.filter === "caramel"}
              onChange={this.props.onChange}
              className="mr-1"
            />
            Caramel
          </label>
        </div>

        <div className="mb-1 font-serif">
          <label>
            <input
              type="radio"
              name="filter"
              value="peanutyalmondy"
              checked={this.props.filter === "peanutyalmondy"}
              onChange={this.props.onChange}
              className="mr-1"
            />
            Nutty
          </label>
        </div>

        <div className="mb-1 font-serif">
          <label>
            <input
              type="radio"
              name="filter"
              value="nougat"
              checked={this.props.filter === "nougat"}
              onChange={this.props.onChange}
              className="mr-1"
            />
            Nougat
          </label>
        </div>

        <div className="mb-1 font-serif">
          <label>
            <input
              type="radio"
              name="filter"
              value="crispedricewafer"
              checked={this.props.filter === "crispedricewafer"}
              onChange={this.props.onChange}
              className="mr-1"
            />
            Crisped rice / wafer
          </label>
        </div>

        <div className="mb-1 font-serif">
          <label>
            <input
              type="radio"
              name="filter"
              value="hard"
              checked={this.props.filter === "hard"}
              onChange={this.props.onChange}
              className="mr-1"
            />
            Hard
          </label>
        </div>

        <div className="mb-1 font-serif">
          <label>
            <input
              type="radio"
              name="filter"
              value="bar"
              checked={this.props.filter === "bar"}
              onChange={this.props.onChange}
              className="mr-1"
            />
            Bar
          </label>
        </div>

        <div className="mb-1 font-serif">
          <label>
            <input
              type="radio"
              name="filter"
              value="pluribus"
              checked={this.props.filter === "pluribus"}
              onChange={this.props.onChange}
              className="mr-1"
            />
            Pieces
          </label>
        </div>
      </div>
    )
  }
}

export default Filter
